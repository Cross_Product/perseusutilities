# Notes on Perseus and Cubical Complexes #

These are notes taken as I work through trying to compute persistence on a cubical complex with Perseus.
This is all done on macOS Mojave (10.14.6).
Perseus is 64-bit, so it should be compatible with macOS Catalina as well.

## Installing Perseus: ##
* Download the executable from http://people.maths.ox.ac.uk/nanda/perseus/index.html 
* Open a terminal, navigate to the directory containing the executable and change permissions, i.e., run `chmod +x perseusMac`.
* Download the example file for **Cubical Toplexes** and put it in the same directory.
* Run Perseus on the example: `./perseusMac cubtop (path to input file) (output file string)` (`./` means to execute the file, cubtop tells Perseus the type of data it should expect.) Specifying an output file string is optional. The default is `output`. However, Perseus will overwrite files without warning, so it is good to provide one.
* This should produce three results: `output.t.txt_0`, `output.t.txt_1`, and `output.t.txt_betti`. 
* Open the output files. The ones ending in `.txt_n` are the birth and death times in _n_-dimensional homology. Each row is a cycle, and the columns are birth and death time. Note that _-1_ means the cycle never died. The file ending in `.txt_betti` gives the betti numbers at each time in the filtration. The first entry is the filtration index, the second is the _0_-th betti number, then the _1_-st betti number, and so on. All of these are only computed up to the dimension of the complex. Only filtration values where changes occur seem to be displayed.
* Do the same with the sparse cubical complex example. Here there should be two eternal components in dimension zero and nothing else.
* Download the file `persdia.m` here: http://people.maths.ox.ac.uk/nanda/source/persdia.m
* This is a simple script to display the Perseus output as a persistence diagram, using Matlab.

## Examples on Toy Data ##

* Use Matlab to load `examples.mat`. This contains meshings of three functions. On the 2D grid _Ω = [-1,1]×[-1,1]_, are the functions: `Z` the hyperbolic paraboloid _x^2 - y^2_, and `W` the inverted parabola _-x^2 - y^2_. On a 3D grid, _Ω = [-1,1]×[-1,1]×[-1,1]_, the variable `W3` contains the data for _-x^2 - y^2 - z^2_.
* To create a sublevel complex, call `complex = sublevelcomplex(Z,.1)`. (The second parameter is the increment between filtration values.)
* Run `persublevel(complex,'test')` to write the complex to a file that Perseus can read.
* Run Perseus on the result using `./perseusMac CubTop test.txt`. (Matlab automatically adds the `.txt` extension.)
* If you used _Z_ you should see two connected components which join at level 9. If you used _W_ you should see four connected components which join at level 9 to form an _H1_ class, which then dies at level 18. For _W3_, there should be eight _H0_ classes, five _H1_s, and one _H2_.
* To generate similar examples, one can use the Matlab command `meshgrid`.
* A complete example is given in `example_script.m`.

## Visualizing Output ##

![Example persistence diagram](example_diagram.png)

The version of `persdia.m` distributed here differs from the original. In particular, it allows for plotting of multiple homological dimensions simultaneously.

* To plot the persistence diagram in a single homological dimension, run `persdia('output_n.txt')` where `output.txt_n` is one of the birth-death files created by Perseus.
* To plot the persistence diagrams of multiple dimensions, run `persdia('output',n)`. Note the incomplete filename. Here `n` is the maximum dimension that should be plotted. Each dimension will be displayed in a different color. Infinite intervals are indicated with a diamond, while normal ones are an asterisk.

## Examples on Real Data ##

The file `readExampleData.m` can load some of the real example data. The following is the persistence diagram of the pagerank dataset, with _superlevel set_ persistence.

![Pagerank persistence diagram](pagerank_superlevel_persistence.png)


## Comments ##

* Currently this code is set up for domains between 2D and 5D. It is easy to hardcode any dimension, however I have not worked out a way to generalize it to arbitrary dimensions. It uses the most naïve search method possible and so will probably slow down significantly as the dimension grows. Should do some research into faster algorithms.
* Currently the data processing is specific to the pagerank example. Either this routine will have to be edited or new ones written for other datasets.
* This, and Perseus in general, only work on top-dimensional cubes. In theory we may want to have lower-dimensional cubes available. We are effectively getting a slightly lower resolution than our mesh theoretically permits.
* Perseus requires data to be in Euclidean space. Some of our data is on periodic domains, e.g. cylinders. How should we deal with this?

