% A simple script showing the pipeline for sublevel persistence with Perseus.
% Define a function on a 3D grid:
v = -1:.1:1;
[X,Y,Z] = meshgrid(v,v,v);
F = -X.^2 - Y.^2 - Z.^2;
% Add some noise:
F = F + .1*rand(size(F));
filename = 'noisy3Dexample';
increment_size = .02;
complex = sublevelcomplex(F,increment_size);
persublevel(complex,filename);
% Call Perseus from the terminal
!./perseusMac CubTop noisy3Dexample.txt noisy3Dexample
% Plot the output
persdia(filename,2);
