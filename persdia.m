function [births,deaths] = persdia2(varargin)
% PERSDIA2 Plots a persistence diagram from Perseus output.
% INPUTS:
%   Required: filename, containing output from Perseus. If specifying only one
%       option, then this should be the entire filename. Otherwise the '_n.txt'
%       portion should be omitted.
%   Optional: max_dim, the maximum dimension of homology to include in the
%       persistence diagram plot. All dimensions less than max_dim will be
%       included.
% OUTPUTS:
%   births, list of birth times, each column a different homological dimension.
%   deaths, list of death times, each column a different homological dimension.

if nargin==0
    n = -1;
    return;
else
    filename = varargin{1};
end    

max_dim = -1;
if nargin == 2
    max_dim = varargin{2};
end

figure;
hold on;
title(filename);
xlabel('birth');
ylabel('death');

% Get birth and death times
if max_dim < 0
    ints = load(filename);
    births = ints(:,1);
    deaths = ints(:,2);
else
    betti_filename = [filename,'_betti.txt'];
    bettis = load(betti_filename);
    max_ints = max(bettis(:,2:end),[],'all')
    births = zeros(max_ints,max_dim+1);
    deaths = zeros(max_ints,max_dim+1);
    for i=0:max_dim
        tmp_filename = [filename,'_',int2str(i),'.txt'];
        ints = load(tmp_filename);
        num_ints = length(ints(:,1));
        births(1:num_ints,i+1) = ints(:,1);
        deaths(1:num_ints,i+1) = ints(:,2);
    end
end

minb = min(births,[],'all');
maxd = max(deaths,[],'all');
if (maxd < 0) 
    maxd = max(births)+1;
end
if (minb == maxd)
    maxd = maxd+1;
end

axis([minb,maxd+(maxd-minb)/20,minb,maxd+(maxd-minb)/20]);
colors = 'rcbgm';

% Reset max_dim so all of the following always works.
for i = 1:max(max_dim+1,1)
    % Extract indices of those intervals which die.
    tmp_deaths = deaths(:,i);
    tmp_births = births(:,i);
    normal_ints = find(tmp_deaths > 0);
    % Extract indices of those intervals which persist throughout.
    inf_ints = find(tmp_deaths == -1);
    infd_vec = (maxd + (maxd-minb)/20)*ones(size(inf_ints));
    % Plot the points.
    plot(tmp_births(normal_ints),tmp_deaths(normal_ints),[colors(i),'*']);
    plot((tmp_births(inf_ints)), infd_vec,[colors(i),'d']);
end

% plot the diagonal
diag = minb:(maxd-minb)/20:maxd;
plot(diag,diag,'k-');
grid on;
hold off;

end
