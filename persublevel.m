function persublevel(subs,filename)
% PERSUBLEVEL writes sublevel set array into Perseus format.
% INPUTS:
%   subs = integer array of sublevel sets, columns correspond to X, rows to Y.
%   filename = file to write output to.
% OUTPUT:
%   Perseus format sublevel set data written to filename.

dimensions = size(subs);
d = length(dimensions);
% Permute the array because we want to travel through rows (x coords) first.
p = 1:d;
p(1)=2; p(2)=1;
subs = permute(subs,p);
output = [d; dimensions'; subs(:)];
writematrix(output,filename,'Delimiter','space');

end
