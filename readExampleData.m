function data = readExampleData(varargin)
% READEXAMPLEDATA
% INPUT: variable - filename mandatory
%   varargin{1} = filename. Name of data file to read.
%   varargin{2} = vector with the dimensions of the data set, that is, number
%       of data points in the x,y,z,... directions.
%   varargin{3} = dimension of data set.
%   varargin{4} = file format (NOT IMPLEMENTED). Default is assumed to be a 
%       column matrix in which the first n columns contain the spatial
%       coordinates and the last column contains the function value. The
%       spatial coordinates should be in increasing lex order.
% OUTPUT: data as an n-dimensional array

% Example settings:
%   filename = 3d_pr-r_map_plot_data.dat
%   data_size = [101,100];
%   dimn = 2;

filename = varargin{1};
data_size = varargin{2};
dimn = varargin{3};

all_data = importdata(filename);
input_size = size(all_data);
raw_data = all_data(:,dimn+1);   % pull the last column for 2D data.

% Sanity check
if input_size(1) ~= prod(data_size)
    warning('Data size does not mention dimensions of supplied data.');
    data = -1;
    return;
end

% Process the data to remove NaN, Inf, and such.
% It seems reasonable to replace both with the maximum measured value.
max_val = max(raw_data);
min_val = min(raw_data);
inf_vals = isinf(raw_data);
nan_vals = isnan(raw_data);
raw_data(inf_vals) = max_val;
raw_data(nan_vals) = max_val;

% Want columns to correspond to x values, and rows to y, so swap the first
%   two entries of data size.
data_size = [data_size(:,2),data_size(:,1)];
data = reshape(raw_data,data_size);

end
