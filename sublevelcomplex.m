function sublevels = sublevelcomplex(fnx,level_size)
% SUBLEVELCOMPLEX compute sublevel sets as cubical complexes in Perseus format.
% INPUTS:
%   fnx = n-dimensional array of function values (WARNING: currently only
%       designed for 2-D arrays).
%   level_size = step size for sublevels.
%   level_min = function value for first sublevel set. NOT IMPLEMENTED
%   level_max = function value for last sublevel set. NOT IMPLEMENTED
% OUTPUT:
%   persformat = n-dimensional array of filtration values

% TODO: allow to be set arbitrarily by user.
%level_min = min(fnx);
%level_max = max(fnx);

sublevels = floor(fnx/level_size);  % Computes the pointwise sublevel sets.

dimn=length(size(fnx));
e = [0,1];

% TODO: generalize to n dimensions. Currently implemented only for n=2,3,4.
%   There is probably a more efficient algorithm.
switch dimn
    case 2
        [rows,cols] = size(fnx);
        for j = 1:cols-1
            for i = 1:rows-1
                sublevels(i,j) = max(sublevels(i+e,j+e),[],'all');
            end
        end
        % Cubes are marked by their anchor vertex, so the last row/column do
        %   not correspond to a cube.
        sublevels = sublevels(1:rows-1,1:cols-1);
    case 3
        [rows,cols,pags] = size(fnx);
        for k = 1:pags-1
            for j = 1:cols-1
                for i = 1:rows-1
                    sublevels(i,j,k) = max(sublevels(i+e,j+e,k+e),[],'all');
                end
            end
        end
        sublevels = sublevels(1:rows-1,1:cols-1,1:pags-1);
    case 4
        [rows,cols,pags,shts] = size(fnx);
        for l = 1:shts-1
            for k = 1:pags-1
                for j = 1:cols-1
                    for i = 1:rows-1
                        sublevels(i,j,k,l) = max(sublevels(i+e,j+e,k+e,l+e),[],'all');
                    end
                end
            end
        end
        sublevels = sublevels(1:rows-1,1:cols-1,1:pags-1,1:shts-1);
    case 5
        [rows,cols,pags,shts,bks] = size(fnx);
        for m = 1:bks-1
            for l = 1:shts-1
                for k = 1:pags-1
                    for j = 1:cols-1
                        for i = 1:rows-1
                            sublevels(i,j,k,l,m) = max(sublevels(i+e,j+e,k+e,l+e,m+e),[],'all');
                        end
                    end
                end
            end
        end
        sublevels = sublevels(1:rows-1,1:cols-1,1:pags-1,1:shts-1,1:bks-1);
    otherwise
        error('Detected dimension function of dimension > 5.');
        sublevels = -1;
        return;
end

% Normalize so first sublevel is 1.
sublevels = sublevels - min(sublevels,[],'all') + 1;

end
